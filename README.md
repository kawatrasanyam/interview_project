# A8
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.0.

## Development
`npm start` - web server
`npm run hmr` - hmr web server
`gulp svg` - [svg store](http://wsm-0911:3003/other/svg-store) 
`npm run server` - start local express server

## Build
`npm run build` - build project
`npm run build:server` - static web server watching at ./dist/a8 folder 
`npm run report` - look at bundle

