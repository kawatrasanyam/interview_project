import {Component, ViewEncapsulation} from '@angular/core';
import {questionsUsed} from '../../questions_used';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent {
    i = 0;
    questions = questionsUsed;
    onScreenQues = questionsUsed[this.i];

    saveChange(sno: string) {
        document.getElementById(sno).style.backgroundColor = '#4CAF50';
        document.getElementById(sno).style.color = 'white';
        this.onScreenQues = questionsUsed[++this.i];
    }

    reviewChange(sno) {
        document.getElementById(sno).style.backgroundColor = 'red';
        document.getElementById(sno).style.color = 'white';
        this.onScreenQues = questionsUsed[++this.i];
    }

    loadQues(sno) {
        this.i = sno;
        this.onScreenQues = questionsUsed[this.i];
    }


}



