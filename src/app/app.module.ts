import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DOCUMENT } from '@angular/common'
import { AppComponent } from './app.component';
import {UxButtonModule} from "@netcracker/ux-ng2/library/button";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    UxButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
